<?php

namespace App\Tests;

use App\Entity\User;
use Exception;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TodoListTest extends WebTestCase
{

    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    private $client;
    /**
     * @var \Doctrine\ORM\EntityManager|object|null
     */
    private $em;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $kernel = self::bootKernel();
        $this->em = $kernel->getContainer()->get("doctrine.orm.entity_manager");
    }

    public function testCreate()
    {
        $user = [
            "email" => "a@b107.fr",
            'password' => "azertyui"
        ];
        $this->client->request('POST','/login',$user);
        $this->client->request('POST','/todo', ["title" => "toto".rand(0,999999)]);

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
        $this->assertEquals("TodoList Ajouter", $content);
    }

    public function testCreatetoMany()
    {
        $user = [
            "email" => "a@b154.fr",
            'password' => "azertyui"
        ];

        $this->expectException(Exception::class);
        $this->expectExceptionCode(500);
        $this->expectExceptionMessage("Vous ne devez avoir qu'une todolist");

        $this->client->request('POST','/login',$user);
        $this->client->request('POST','/todo', ["title" => "toto".rand(0,999999)]);
        $content = json_decode($this->client->getResponse()->getContent(), true);

    }

    public function testCreateNotLog()
    {
        $this->client->request('POST','/todo', ["title" => "toto".rand(0,999999)]);

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
        $this->assertEquals("Authentifier vous", $content);
    }

    public function testCreateItemSuccess()
    {
        $user = [
            "email" => "a@b107.fr",
            'password' => "azertyui"
        ];

        $this->client->request('POST','/login',$user);
        $this->client->request('POST','/todo/item', ["name" => "tptp".rand(0,80000), "content" => "Toto"]);

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
        $this->assertEquals("Item Ajouter", $content);
    }

    public function testCreateItemToFast()
    {
        $user = [
            "email" => "a@b107.fr",
            'password' => "azertyui"
        ];

        $this->expectException(Exception::class);
        $this->expectExceptionCode(500);
        $this->expectExceptionMessage("Vous devez attendre 30 minutes entre chaque ajout");

        $this->client->request('POST','/login',$user);
        $this->client->request('POST','/todo/item', ["name" => "tptp".rand(0,80000), "content" => "Toto"]);
    }

    public function testCreateItemToMuch()
    {
        $user = [
            "email" => "a@b154.fr",
            'password' => "azertyui"
        ];

        $this->expectException(Exception::class);
        $this->expectExceptionCode(500);
        $this->expectExceptionMessage("10 items maximum");

        $this->client->request('POST','/login', $user);

        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneBy(["email" => "a@b107.fr"]);
        $this->em->remove($user->getTodoList());
        $this->em->flush();

        $this->client->request('POST','/todo/item', ["name" => "tptp".rand(0,80000), "content" => "Toto"]);
        $content = json_decode($this->client->getResponse()->getContent(), true);
    }
}
