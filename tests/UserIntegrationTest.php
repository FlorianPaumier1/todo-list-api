<?php

namespace App\Tests;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use PHPUnit\Framework\TestCase;

class UserIntegrationTest extends WebTestCase
{

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    protected static $urlRegister = "http://localhost:8000/register";
    protected static $urlLogin = "http://localhost:8000/login";
    /**
     * @var
     */
    private $client;

    private $headers = ['Accept' => 'application/json', 'Content-Type' => 'application/json', 'exceptions' => false, "form_params" => ""];

    public $logUser = [];

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->client->request('GET','/logout');
    }

    public function testUserSuccess()
    {
        $random = rand(1, 500000);
        $user = [
            "age" => 13,
            "email" => "a@b$random.fr",
            'firstName' => "first",
            'lastName' => "last",
            'password' => "azertyui"
        ];


        $this->client->request('POST','/register',$user);

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
        $this->assertEquals("L'utilisateur à bien été créé", $content);

        $this->logUser = [
            "email" => "a@b$random.fr",
            'password' => "azertyui"
        ];
    }

    public function testUserAlreadyExist()
    {
        $user = [
            "age" => 13,
            "email" => "a@a.fr",
            'firstName' => "first",
            'lastName' => "last",
            'password' => "azertyui"
        ];

        $this->client->request('POST','/register',$user);

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());
        $this->assertEquals("L'utilisateur existe déja", $content);
    }

    public function testUserFailed()
    {
        $user = [
            "age" => 12,
            "email" => "a@a.f",
            'firstName' => "",
            'lastName' => "",
            'password' => "azert"
        ];

        $this->client->request('POST','/register',$user);

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());
        $this->assertEquals([
            "age" => "Vous devez avoir plus de 13 ans",
            "firstName" => "Le prénom doit être renseigné",
            "lastName" => "Le nom doit être renseigné",
            "password" => "Votre mot de passe doit faire entre 8 et 40 caractères"
        ], $content);
    }

    public function testUserPasswordTooLong()
    {
        $random = rand(0, 500);
        $user = [
            "age" => 13,
            "email" => "b$random@a.fr",
            'firstName' => "a",
            'lastName' => "a",
            'password' => "azesddddddddssssssssssssddddddddddddddssssrt"
        ];

        $this->client->request('POST','/register',$user);

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());
        $this->assertEquals([
            "password" => "Votre mot de passe doit faire entre 8 et 40 caractères"
        ], $content);
    }

    public function testLoginSuccess()
    {
        $this->client->request('POST','/login',$this->logUser);

        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());
    }

    public function testLogoutSuccess()
    {
        $this->client->request('GET','/logout');

        $content = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());
    }

    public function testLoginWrongEmail()
    {
        $user = [
            "email" => "a@aa.fr",
            'password' => "azertyui"
        ];


        $this->client->request('POST','/login',$user);

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());
    }

    public function testLoginWrongPassword()
    {
        $user = [
            "email" => "a@a.fr",
            'password' => "azertdyui"
        ];


        $this->client->request('POST','/login',$user);

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());
    }
}
