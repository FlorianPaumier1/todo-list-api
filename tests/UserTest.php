<?php

namespace App\Tests;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserTest extends WebTestCase
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    protected function setUp(): void
    {
        static::bootKernel();
        $this->em = self::$kernel->getContainer()->get('doctrine.orm.entity_manager');
    }

    public function testUserSuccess()
    {
        $user = (new User())
            ->setAge(13)
            ->setEmail("a@a.fr")
            ->setFirstName("first")
            ->setLastName("last")
            ->setPassword("azertyui")
        ;

        $isValid = $user->isValid();

        self::assertEquals([], $isValid);

        if (!$this->em->getRepository(User::class)->findOneBy(["email" => $user->getEmail()]))
            $this->em->persist($user);
            $this->em->flush();

    }

    public function testUserAlreadyExist()
    {
        $user = (new User())
            ->setAge(13)
            ->setEmail("a@a.fr")
            ->setFirstName("first")
            ->setLastName("last")
            ->setPassword("azertyui")
        ;

        $isValid = $user->isValid();

        self::assertEquals([], $isValid);

        self::assertNotNull($this->em->getRepository(User::class)->findOneBy(["email" => $user->getEmail()]));
    }

    public function testUserTooYoung()
    {
        $user = (new User())
            ->setAge(12)
            ->setEmail("a@a.fr")
            ->setFirstName("first")
            ->setLastName("last")
            ->setPassword("azertyui")
        ;

        $isValid = $user->isValid();

        self::assertEquals(["age" => "Vous devez avoir plus de 13 ans"], $isValid);
    }

    public function testUserEmailNotValid()
    {
        $user = (new User())
            ->setAge(13)
            ->setEmail("aa.fr")
            ->setFirstName("first")
            ->setLastName("last")
            ->setPassword("azertyui")
        ;

        $isValid = $user->isValid();

        self::assertEquals(["email" => "Votre email n'est pas valide"], $isValid);
    }

    public function testUserPasswordTooShort()
    {
        $user = (new User())
            ->setAge(13)
            ->setEmail("a@a.fr")
            ->setFirstName("first")
            ->setLastName("last")
            ->setPassword("azer")
        ;

        $isValid = $user->isValid();

        self::assertEquals(["password" => "Votre mot de passe doit faire entre 8 et 40 caractères"], $isValid);
    }

    public function testUserPasswordTooLong()
    {
        $user = (new User())
            ->setAge(13)
            ->setEmail("a@a.fr")
            ->setFirstName("first")
            ->setLastName("last")
            ->setPassword("azerqegbubLERBDBdlfkjbvldjfbgljbfljdfbvljd")
        ;

        $isValid = $user->isValid();

        self::assertEquals(["password" => "Votre mot de passe doit faire entre 8 et 40 caractères"], $isValid);
    }

    public function testUserFirstNameEmpty()
    {
        $user = (new User())
            ->setAge(13)
            ->setEmail("a@a.fr")
            ->setLastName("last")
            ->setPassword("azertyui")
        ;

        $isValid = $user->isValid();

        self::assertEquals(["firstName" => "Le prénom doit être renseigné"], $isValid);
    }

    public function testUserLastNameEmpty()
    {
        $user = (new User())
            ->setAge(13)
            ->setEmail("a@a.fr")
            ->setFirstName("last")
            ->setPassword("azertyui")
        ;

        $isValid = $user->isValid();

        self::assertEquals(["lastName" => "Le nom doit être renseigné"], $isValid);
    }
}
