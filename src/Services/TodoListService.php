<?php


namespace App\Services;


use App\Entity\TodoList;
use App\Entity\TodoListItem;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class TodoListService
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    private $mailer;

    public function __construct(EntityManagerInterface $em, EmailServices $mailer)
    {
        $this->em = $em;
        $this->mailer = $mailer;
    }

    /**
     * @param User $user
     * @return User
     */
    public function create(User $user, TodoList $todoList)
    {
        if (!empty($user->getTodoList())) throw new Exception("Vous ne devez avoir qu'une todolist", 500);
        $user->setTodoList($todoList);

        return $user;
    }

    public function insert(TodoList $list, TodoListItem $todoListItem)
    {

        if ($this->em->getRepository(TodoListItem::class)->findOneBy(["name" => $todoListItem->getName()])) throw new Exception("Un item avec ce nom exist déjà", 500);
        if (strlen($todoListItem->getContent()) > 1000) throw new Exception("Le contenu doit être inférieure à 1000 caractères", 500);
        if ($list->getTodoListItems()->count() === 10) throw new Exception("10 items maximum", 500);

        if ($list->getTodoListItems()->count() > 0) {
            /**
             * @var $last TodoListItem
             */
            $last = $list->getTodoListItems()->first();

            foreach ($list->getTodoListItems() as $listItem){
                if ($listItem->getCreatedAt() > $last->getCreatedAt()) $last = $listItem;
            }

            $diffMinute = (int)date_diff($todoListItem->getCreatedAt(), $last->getCreatedAt())->format("%i");
            $diffHours = (int)date_diff($todoListItem->getCreatedAt(), $last->getCreatedAt())->format("%h");

            if ($diffMinute < 30 && $diffHours === 0) throw new Exception("Vous devez attendre 30 minutes entre chaque ajout", 500);
        }

        if ($list->getTodoListItems()->count() === 7) {
            $this->mailer->limitTodo($list->getAuthor());
        }


        $list->addTodoListItem($todoListItem);
        return $list;
    }
}
