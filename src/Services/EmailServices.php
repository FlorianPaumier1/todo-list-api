<?php


namespace App\Services;


use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Message;
use Symfony\Component\Mime\RawMessage;

class EmailServices
{


    private $form = "nopreply@cms.fr";

    /** @var MailerInterface */
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function limitTodo(User $user)
    {
        $message = (new TemplatedEmail())
            ->from($this->form)
            ->to($user->getEmail())
            ->subject('Limite de todo')
            ->htmlTemplate("mailer/todo.html.twig")
            ->context([
                "user" => $user,
            ])
        ;

        return $this->send($message);
    }


    private function send(RawMessage $message){
        try {
            $this->mailer->send($message);
        }catch (TransportExceptionInterface $e){
            return [
                "error" => true,
                "message" => $e->getMessage()
            ];
        }

        return ["error" => false];
    }
}
