<?php


namespace App\Controller;


use App\Entity\TodoList;
use App\Entity\TodoListItem;
use App\Entity\User;
use App\Form\TodoItemType;
use App\Form\TodoType;
use App\Services\TodoListService;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/todo",name="test")
 * Class TodoController
 * @package App\Controller
 */
class TodoController extends AbstractController
{

    /**
     * @Route("")
     * @param Request $request
     * @param TodoListService $todoList
     * @return JsonResponse
     * @throws \Exception
     */
    public function createTodo(Request $request, EntityManagerInterface $em, TodoListService $todoList, SerializerInterface $serializer)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user){
            return new JsonResponse("Authentifier vous", 401);
        }
        $item = new TodoList();
        $form = $this->createForm(TodoType::class, $item);
        $form->submit($request->request->all());

        if ($form->isValid()){
            $todo = $todoList->create($user, $item);
            $em->persist($todo);
            $em->flush();

            return new JsonResponse("TodoList Ajouter", 201);
        }

        return new JsonResponse($serializer->serialize($form->getErrors(), 'json'), 500);
    }

    /**
     * @Route("/item")
     * @param Request $request
     * @param TodoListService $todoList
     * @param EntityManagerInterface $em
     * @return JsonResponse
     * @throws \Exception
     */
    public function addItem(Request $request, TodoListService $todoList, EntityManagerInterface $em, SerializerInterface $serializer)
    {
        $user = $this->getUser();

        if (!$user){
            return new JsonResponse("Authentifier vous", 401);
        }

        $item = new TodoListItem();
        $form = $this->createForm(TodoItemType::class, $item);
        $form->submit($request->request->all());

        if ($form->isValid()){
            $todoList->insert($user->getTodoList(), $item);
            $em->flush();

            return new JsonResponse("Item Ajouter", 201);
        }

        return new JsonResponse($serializer->serialize($form->getErrors(), 'json'), 500);
    }
}
