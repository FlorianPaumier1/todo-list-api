<?php


namespace App\Controller;


use App\Entity\User;
use App\Form\RegistrationType;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends ApiController
{

    /**
     * @Route("/")
     * @return Response
     */
    public function home()
    {
        return new Response();
    }
    /**
     * @Route("/register", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function register(
        Request $request,
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder,
        SerializerInterface $serializer
    ) {
        $request = $this->transformJsonBody($request);

        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            if ($entityManager->getRepository(User::class)->findOneBy(["email" => $user->getEmail()])) {
                return new JsonResponse("L'utilisateur existe déja",500);
            }

            $isValid = $user->isValid();

            if (!empty($isValid)) {
                return new JsonResponse($isValid,500);
            }

            $user->setPassword(
                $passwordEncoder->encodePassword($user, $user->getPassword())
            );

            $entityManager->persist($user);
            $entityManager->flush();

            return new JsonResponse("L'utilisateur à bien été créé", 201);
        }

        return new JsonResponse($serializer->serialize($form->getErrors(), 'json'),500);
    }
}
