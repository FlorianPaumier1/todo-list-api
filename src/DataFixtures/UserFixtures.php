<?php

namespace App\DataFixtures;

use App\Entity\TodoList;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = (new User())
            ->setAge(13)
            ->setPassword("azertyui")
            ->setFirstName("first")
            ->setLastName("last")
            ->setEmail("email@email.fr");

        $manager->persist($user);

        $user2 = (new User())
            ->setAge(13)
            ->setPassword("azertyui")
            ->setFirstName("first")
            ->setLastName("last")
            ->setEmail("email@email2.fr");

        $todo = new TodoList();

        $user2->setTodoList($todo->setTitle("Title"));

        $manager->persist($user2);
        $manager->flush();
    }
}
